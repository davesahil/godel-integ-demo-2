package in.juspay.godelintegdemo2;

import android.util.Log;
import android.webkit.WebView;
import in.juspay.godel.browser.JuspayWebViewClient;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.JuspayWebView;

public class CustomWebViewClient extends JuspayWebViewClient {

    private static final String LOG_TAG = CustomWebViewClient.class.getName();
    public CustomWebViewClient(JuspayWebView webView, JuspayBrowserFragment browserFragment){
        super(webView, browserFragment);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.d(LOG_TAG, "shouldOverrideUrlLoading - "+url);
        return false;
    }


}
