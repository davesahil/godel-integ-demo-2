package in.juspay.godelintegdemo2;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import in.juspay.godel.core.Card.CardType;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.JuspayBrowserFragment.JuspayWebviewCallback;
import in.juspay.godel.ui.JuspayWebView;

public class MainActivity extends ActionBarActivity {

    private BrowserFragment browserFragment;
    private static final String URL = "http://www.makemytrip.com";
    private static final String LOG_TAG = MainActivity.class.getName();
    public static final String BROWSER_FRAGMENT = "browserFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(LOG_TAG, "is savedInstanceState NULL ? "+(savedInstanceState == null));
        if(savedInstanceState != null) {
            browserFragment = (BrowserFragment) getSupportFragmentManager().getFragment(savedInstanceState, BROWSER_FRAGMENT);
        } else {
            showJBFragment();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Put the lastFragment in the outState Bundle
        Log.d(LOG_TAG, "Saving browserFragment");
        getSupportFragmentManager().putFragment(outState, BROWSER_FRAGMENT, browserFragment);
        super.onSaveInstanceState(outState);
    }


    private void showJBFragment() {
        browserFragment = new BrowserFragment();
        browserFragment.setupJuspayWebviewCallbackInterface(webviewCallback);

        Bundle args = new Bundle();
        args.putString("displayNote", "Recharge 9620917775 with RS 10");
        args.putString("remarks", "Recharge 9620917775");
        args.putString("merchantId", "juspay_recharge_netbanking");
        args.putString("clientId", "juspay_recharge");
        args.putString("transactionId", "netbanking_demo_transaction_id");
        args.putString("url", URL);
        args.putBoolean("progressDialogEnabled", true);
        args.putSerializable("card_type", CardType.CREDIT_CARD);
        browserFragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, browserFragment, browserFragment.getClass().getSimpleName());
        transaction.commit();
    }

    JuspayWebviewCallback webviewCallback = new JuspayWebviewCallback() {
        @Override
        public void webviewReady() {
            JuspayWebView webView = browserFragment.getWebView();
            webView.setWebViewClient(new CustomWebViewClient(webView, browserFragment));
        }
    };

    public static class BrowserFragment extends JuspayBrowserFragment {

        public BrowserFragment() {
        }
    }

}
